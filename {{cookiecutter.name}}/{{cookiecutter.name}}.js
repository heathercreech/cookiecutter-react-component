import React, { Component } from 'react';
import PropTypes from 'prop-types';


{% if cookiecutter.class == 'y' %}
export class {{ cookiecutter.name }} extends Component {

    render() {
        return (
            <div>
                A Component
            </div>
        );
    }

}
{%- else -%}
export const {{ cookiecutter.name }} = (props) => (

);
{%- endif %}
